//
//  MusicPlayer.swift
//  MusicPlayer
//
//  Created by rafiul hasan on 9/12/21.
//

import Foundation

class MusicPlayer: ObservableObject {
    @Published var currentSong: Song?
    
    func pressButton(song: Song) {
        if currentSong == song {
            currentSong = nil
        } else {
            currentSong = song
        }
    }
}

