//
//  MiniPlayerView.swift
//  MusicPlayer
//
//  Created by rafiul hasan on 9/12/21.
//

import SwiftUI

struct MiniPlayerView: View {
    @EnvironmentObject private var musicPlayer: MusicPlayer
    
    var body: some View {
        if let currentSong = musicPlayer.currentSong {
            SongView(song: currentSong)
                .padding(.all, 24)
        } else {
            EmptyView()
        }
    }
}

struct MiniPlayerView_Previews: PreviewProvider {
    static var previews: some View {
        MiniPlayerView()
    }
}
