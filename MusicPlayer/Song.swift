//
//  Song.swift
//  MusicPlayer
//
//  Created by rafiul hasan on 9/12/21.
//

import Foundation

struct Song: Identifiable, Equatable {
    var id = UUID()
    let artist: String
    let name: String
    let cover: String
}
