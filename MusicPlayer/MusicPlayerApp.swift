//
//  MusicPlayerApp.swift
//  MusicPlayer
//
//  Created by rafiul hasan on 9/12/21.
//

import SwiftUI

@main
struct MusicPlayerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(MusicPlayer())
        }
    }
}
