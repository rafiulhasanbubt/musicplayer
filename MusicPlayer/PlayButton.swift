//
//  PlayButton.swift
//  MusicPlayer
//
//  Created by rafiul hasan on 9/12/21.
//

import SwiftUI

struct PlayButton: View {
    @EnvironmentObject private var musicPlayer: MusicPlayer
    let song: Song
    
    private var buttonText: String {
        if song == musicPlayer.currentSong {
            return "stop"
        } else {
            return "play"
        }
    }
    
    var body: some View {
        Button {
            musicPlayer.pressButton(song: song)
        } label: {
            Image(systemName: buttonText)
                .font(.system(.largeTitle))
                .foregroundColor(.black)
        }
    }
}

struct PlayButton_Previews: PreviewProvider {
    static var previews: some View {
        PlayButton(song: Song(artist: "", name: "", cover: ""))
    }
}
